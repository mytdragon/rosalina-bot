const axios = require('axios');
const config = require('../config.json');

const getUser = function (data) {
    return axios({
        method: 'get',
        url: config['mk8d-timetrial-url'] + '/users',
        headers: {
            'Accept': 'application/json',
            'Content-Type': 'application/json',
            'Authorization': 'Bearer ' + config['mk8d-timetrial-token']
        },
        params: data
    })
        .then(function (response) {
            return response.data.data[0];
        })
        .catch(function (error) {
            console.log(error);
        });
}

module.exports = {
    getUser
}
