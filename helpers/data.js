const fs = require('fs');

const config = require('../config.json');

const husPath = './data/handsup.json';

const getHandsup = function () {
    if (!fs.existsSync(husPath)) {
        fs.writeFileSync(husPath, '{}');
    }
    let rawdata = fs.readFileSync(husPath);
    return JSON.parse(rawdata);
}

const saveHandsup = function (hus) {
    if (!fs.existsSync(husPath)) {
        fs.writeFileSync(husPath, '{}');
    }
    fs.writeFileSync(husPath, JSON.stringify(hus));
}

module.exports = {
    getHandsup,
    saveHandsup
}
