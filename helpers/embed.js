const Discord = require('discord.js');
const config = require('../config.json');
const countryFlagEmoji = require("country-flag-emoji");

const footer = {
    text: "© MYT"
};

const profile = function (user, avatar = null, isMkc = false) {
    let fields = [];
    if (user.switch_fc && isMkc) fields.push({name: 'Switch FC', value: user.switch_fc })
    else fields.push({name: 'Switch FC', value: user.switch_fc ? user.switch_fc : `None (Set this with \`${config.prefix}setfc <switch_fc>\`)`})
    if (user.mktour_fc) fields.push({name: 'MKTour FC', value: user.mktour_fc})
    if (user.discord_tag && user.discord_privacy === 'public') fields.push({name: 'Discord', value: user.discord_tag})
    if (user.is_banned !== undefined && user.is_banned === true) fields.push({name: 'Banned', value: user.ban_reason})
    if (user.registered_at_human) fields.push({name: `${user.is_supporter ? 'Site supporter r' : 'R' }egistered at`, value: user.registered_at_human})

    return {
        embed: {
            color: 0x168ede,
            title: `${countryFlagEmoji.get(isMkc ? user.country_code : user.country.code).emoji} ${isMkc ? user.display_name : user.pseudo}`,
            thumbnail: {
		      url: avatar
		    },
            description: user.profile_message ? user.profile_message : null,
            fields: fields,
            timestamp: new Date(),
            footer: footer
        }
    };
};

const notRegisteredError = function () {
    return {
        embed: {
            color: 0xf50076,
            title: 'Not registered',
            description: `You must be registered to use that command.\nYou can register by using \`${config.prefix}register -pseudo <pseudo> -country <code|name>\`.`,
            timestamp: new Date(),
            footer: footer
        }
    };
};

const guildNotAuthorizedError = function () {
    return {
        embed: {
            color: 0xf50076,
            title: 'Not registered',
            description: `Your server is not authorized to use this feature.`,
            timestamp: new Date(),
            footer: footer
        }
    };
};

const singleError = function (title, message) {
    return {
        embed: {
            color: 0xf50076,
            title: title,
            description: message,
            timestamp: new Date(),
            footer: footer
        }
    };
};

const validationError = function (title, error) {
    // Format errors
    var fields = [];
    Object.keys(error.errors).forEach(field => {
        fields.push({
            name: field,
            value: error.errors[field].join('\n')
        })
    });

    return {
        embed: {
            color: 0xf50076,
            title: title,
            description: error.message,
            fields: fields,
            timestamp: new Date(),
            footer: footer
        }
    };
};

const successfull = function (title, message) {
    return {
        embed: {
            color: 0x168ede,
            title: title,
            description: message,
            timestamp: new Date(),
            footer: footer
        }
    };
};

const teams = function (teams) {
    fields = teams.map((team, index ) => {
        return {
            name: `Team ${index + 1}`,
            value: `${team.join(' ')}`
        }
    });

    return {
        embed: {
            color: 0x168ede,
            title: 'Teams générées',
            fields: fields.items,
            timestamp: new Date(),
            footer: footer
        }
    };
};

const timetrial = function (tt) {
    let combo = (tt.character ? `${client.emojis.cache.find(emoji => emoji.name === tt.character.name.replace(/[ \-()]+/g, '')) || ''} ${tt.character.name}` + '\n' : '') +
        (tt.body ? `${client.emojis.cache.find(emoji => emoji.name === tt.body.name.replace(/[ \-()]+/g, '')) || ''} ${tt.body.name}` + '\n' : '') +
        (tt.tire ? `${client.emojis.cache.find(emoji => emoji.name === tt.tire.name.replace(/[ \-()]+/g, '')) || ''} ${tt.tire.name}` + '\n' : '') +
        (tt.glider ? `${client.emojis.cache.find(emoji => emoji.name === tt.glider.name.replace(/[ \-()]+/g, '')) || ''} ${tt.glider.name}` : '');

    let splits = tt.splits ? tt.splits.map( (element, key) => {
        return `${key + 1}. ${element.time}`;
    }).join('\n') : '';

    let fields = [
        {
            name: "Player",
            value: `${countryFlagEmoji.get(tt.user.country.code).emoji} ${tt.user.pseudo}`
        },
        {
            name: "Time",
            value: tt.time
        },
        {
            name: "Combo",
            value: combo ? combo : 'n/a'
        },
        {
            name: "Splits",
            value: splits ? splits : 'n/a'
        }
    ];

    if (tt.video) fields.push({name: 'Video', value: tt.video})

    return {
        embed: {
            color: 0x168ede,
            /*thumbnail: {
	              url: `${config['mk8d-timetrial-web-url']}/images/tracks/${tt.track.code}`
            },*/
            image: {
        		 url: `${config['mk8d-timetrial-web-url']}/images/tracks/${tt.track.code}`
        	},
            title: `[${tt.cc}cc] ${tt.track.name}`,
            fields: fields,
            timestamp: new Date(),
            footer: footer
        },
    };
};

const handsup = function (hu) {
    let fields = [];
    for (let schedule in hu.schedules) {
        let maybes = hu.schedules[schedule].maybe.map(function (player)  {
            return `(${player})`;
        });
        let alternates = hu.schedules[schedule].alternate.map(function (player)  {
            return `[${player}]`;
        });
        let total = hu.schedules[schedule].can.length + maybes.length + alternates.length;
        let paranthesis = maybes.length + alternates.length;
        let required = 6 - hu.schedules[schedule].can.length;
        fields.push({
            name: `${schedule}@${required >= 0 ? required : 0}${paranthesis ? `(${paranthesis})` : ''}${hu.schedules[schedule].opponent ? ` vs ${hu.schedules[schedule].opponent}` : '' }${hu.schedules[schedule].host ? ` Host ${hu.schedules[schedule].host}` : ''}${hu.schedules[schedule].open ? ` Open ${hu.schedules[schedule].open}` : ''}`,
            value: `> ${total > 0 ? `${hu.schedules[schedule].can.join(' ')}${maybes.length ? ` ${maybes.join(' ')}` : ''}${alternates.length ? ` ${alternates.join(' ')}` : ''}` : 'n/a'}`
        });
    }
    fields.sort((a, b) => a.name.localeCompare(b.name) );

    return {
        embed: {
            color: 0x168ede,
            title: 'Wars',
            fields: fields,
            timestamp: new Date(),
            footer: footer
        }
    };
}

module.exports = {
    profile,
    notRegisteredError,
    guildNotAuthorizedError,
    singleError,
    validationError,
    successfull,
    teams,
    timetrial,
    handsup
};
