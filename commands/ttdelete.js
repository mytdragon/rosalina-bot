const userHelper = require("../helpers/user.js");
const { singleError, successfull } = require("../helpers/embed.js");
const config = require('../config.json');

const axios = require('axios');
const collect = require('collect.js');

module.exports = {
	name: 'ttdelete',
	aliases: ['ttdel', 'ttremove', 'ttr'],
	description: 'Delete the last timetrial added for the specified track and cc.',
	usage: '-track <code|name> -cc <150|200>',
	args: true,
	cooldown: 10,
	example: '$ttdelete -cc 150 -track dnbc',
	async execute(message, args) {
		if (!args['track']) return message.channel.send( singleError('Argument missing', 'Argument `track` is required.') );
		if (!args['cc']) return message.channel.send( singleError('Argument missing', 'Argument `cc` is required.') );

		let user = await userHelper.getUser({discord_id: message.author.id});
		if (!user) {
            message.channel.send(embedHelper.notRegisteredError());
			return;
		}

		let tt = await axios({
			method: 'get',
			url: config['mk8d-timetrial-url'] + '/timetrials',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + config['mk8d-timetrial-token']
			},
			params: {
				cc: args['cc'],
				tracks: args['track'],
				users: user.id
			}
		})
			.then(function(response) {
				if(response.data.data.length == 0) {
					message.channel.send( singleError('No timetrial registered', 'There is no timetrial registered for that track and cc.') );
					return false;
				}

				return timesFilter = collect(response.data.data)
					.sortByDesc('created_at')
					.first();
			})
			.catch(function (error) {
				message.channel.send( embedHelper.singleError(
					'Oops...',
					'An unknown error has occured while retrieving your timetrial to edit.'
				));
				return false;
            });

		if (!tt) return;

		axios({
			method: 'delete',
			url: config['mk8d-timetrial-url'] + '/timetrials/' + tt.id,
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + config['mk8d-timetrial-token']
			}
		})
			.then(function(response) {
				message.channel.send( successfull(
					"Success",
					"Timetrial successfully deleted."
				));
			})
			.catch(function (error) {
				let title = 'Cannot delete the time';
                switch (error.response.status) {
					case 404:
                        message.channel.send( singleError(title, error.response.data.message) );
                        break;
                    default:
						message.channel.send( singleError(
							title,
							'An unknown error has occured while deleting the timetrial.'
						));
                }
            });
	}
};
