const Discord = require('discord.js');

const { handsup: handsupConfig } = require('../config.json');
const { handsup, guildNotAuthorizedError, singleError } = require("../helpers/embed.js");
const { getHandsup, saveHandsup } = require("../helpers/data.js");

module.exports = {
	name: 'huclear',
    aliases: ['clear'],
	description: 'Clear schedules for the hands up feature.',
	example: '$clear',
	async execute(message, args) {
		// Check if server is authorized
		let guild = message.guild.id;
		if (!handsupConfig.authorizedGuilds.includes(guild)) {
			message.channel.send( guildNotAuthorizedError() );
			return;
		}

		// Get handsup for the server
		let hus = getHandsup();
		let hu = hus[guild];
		if (hu === undefined || hu.length === 0) {
			message.channel.send( singleError('No schedule set', 'You can set schedule with the command `huset`. Use help to get more details.') );
			return;
		}

		// Clear all schedules
		for (schedule in hu.schedules) {
			hu.schedules[schedule] = {
				"can": [],
				"maybe": [],
				"alternate": [],
				"opponent": null,
				"host": null,
				"open": null
			};

			// Remove role
			let role = message.guild.roles.cache.find(role => role.name === schedule);
			if (role !== undefined) {
				await role.delete({data: {name: schedule}})
					.catch(console.error)
			}

			// Create role
			message.guild.roles.create({data: {name: schedule, mentionable: true}})
				.catch(console.error)
		}

		// Show new hu and save message before deleting past message
		message.channel.send( handsup(hu) )
			.then(newMessage => {
				if (hu.last_message === undefined) hu.last_message = {};
				if (hu.last_message[message.channel.id] !== undefined) {
					message.channel.messages.fetch(hu.last_message[message.channel.id])
						.then(oldMessage => {
							oldMessage.delete();
						});
				}
				hu.last_message[message.channel.id] = newMessage.id;

				// Save the new handsup
				hus[guild] = hu;
				saveHandsup(hus);
			});

		// Remove author's message
		message.delete();
	},
};
