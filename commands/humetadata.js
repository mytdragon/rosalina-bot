const Discord = require('discord.js');

const { handsup: handsupConfig } = require('../config.json');
const { handsup, guildNotAuthorizedError, singleError } = require("../helpers/embed.js");
const { getHandsup, saveHandsup } = require("../helpers/data.js");

module.exports = {
	name: 'humetadata',
    aliases: ['humd', 'md'],
	description: 'Set the opponent, host and when it is open for a schedule.',
	usage: '<schedule> <opponent> <host> <open>',
	example: 'md 20 Pm 0000-0000-000 :00',
	args: true,
	async execute(message, args) {
		// Check if server is authorized
		let guild = message.guild.id;
		if (!handsupConfig.authorizedGuilds.includes(guild)) {
			message.channel.send( guildNotAuthorizedError() );
			return;
		}

		// Check args
		if (args.length != 4) return message.channel.send( singleError('Argument(s) missing', `Arguments \`<schedule> <opponent> <host> <open>\` in following order required.`) );

		// Get handsup for the server
		let hus = getHandsup();
		let hu = hus[guild];
		if (hu === undefined || hu.length === 0) {
			message.channel.send( singleError('No schedule set', 'You can set schedule with the command `huset`. Use help to get more details.') );
			return;
		}

		// Check if schedule exists
		if (hu.schedules[args[0]] === undefined) {
			message.channel.send( singleError(`Schedule ${schedule} not set`, 'You can set schedule with the command `huset`. Use help to get more details.') );
			return;
		}

		// Add the metadatas to the schedule
		hu.schedules[args[0]].opponent = args[1];
		hu.schedules[args[0]].host = args[2];
		hu.schedules[args[0]].open = args[3];

		// Show new hu and save message before deleting past message
		message.channel.send( handsup(hu) )
			.then(newMessage => {
				if (hu.last_message === undefined) hu.last_message = {};
				if (hu.last_message[message.channel.id] !== undefined) {
					message.channel.messages.fetch(hu.last_message[message.channel.id])
						.then(oldMessage => {
							oldMessage.delete();
						});
				}
				hu.last_message[message.channel.id] = newMessage.id;

				// Save the new handsup
				hus[guild] = hu;
				saveHandsup(hus);
			});

		// Remove author's message
		message.delete();
	},
};
