const userHelper = require("../helpers/user.js");
const embedHelper = require("../helpers/embed.js");
const config = require('../config.json');

const axios = require('axios');

module.exports = {
	name: 'profile',
	aliases: ['me'],
	description: 'Show your profile.',
	async execute(message, args) {

		if (args.length) {
			let data = {};
			let avatar = null;

			if (/^\d+$/.test(args[0])) data = { discord_id: args[0] }
			else if (message.mentions.users.first()) {
				data = { discord_id: message.mentions.users.first().id };
				avatar = message.mentions.users.first().avatarURL();
			}
			else data = { pseudo: args[0] }

			let user = await userHelper.getUser(data);
			return user ? message.channel.send( embedHelper.profile(user, avatar) )
				: message.channel.send( embedHelper.singleError('User not found', 'The user you are looking for is maybe not registered or you mistyped the pseudo.') );
		}

        let user = await userHelper.getUser({discord_id: message.author.id});
		return user ? message.channel.send( embedHelper.profile(user, message.author.avatarURL()) )
			: message.channel.send(embedHelper.notRegisteredError());
	},
};
