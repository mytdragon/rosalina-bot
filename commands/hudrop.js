const Discord = require('discord.js');

const { handsup: handsupConfig } = require('../config.json');
const { handsup, guildNotAuthorizedError, singleError } = require("../helpers/embed.js");
const { getHandsup, saveHandsup } = require("../helpers/data.js");

module.exports = {
	name: 'hudrop',
    aliases: ['drop', 'd'],
	description: 'Drop to some schedules.',
	usage: '<schedule> ... <schedule>',
	example: '$d 20 21',
	args: true,
	async execute(message, args) {
		// Check if server is authorized
		let guild = message.guild.id;
		if (!handsupConfig.authorizedGuilds.includes(guild)) {
			message.channel.send( guildNotAuthorizedError() );
			return;
		}

		// Check if tag role exists
		let tagRole = message.guild.roles.cache.find(role => role.name === 'hutag');
		if (tagRole === undefined) await message.guild.roles.create({data: {name: 'hutag', mentionable: true}}).catch(console.error);

		// Get player
		let player = message.member;
		if (message.mentions.members.first()) {
			player = message.mentions.members.first();
			args.splice(0, 1);
		}

		// Get handsup for the server
		let hus = getHandsup();
		let hu = hus[guild];
		if (hu === undefined || hu.length === 0) {
			message.channel.send( singleError('No schedule set', 'You can set schedule with the command `huset`. Use help to get more details.') );
			return;
		}

		// We use this var in case one the schedule isn't full and require ping
		let ping = false;

		// Remove the player from the schedule
		for (schedule of args) {
			if (hu.schedules[schedule] === undefined) {
				message.channel.send( singleError(`Schedule ${schedule} not set`, 'You can set schedule with the command `huset`. Use help to get more details.') );
				return;
			}

			// Find where the player raised the hand and drop him
			let index = hu.schedules[schedule].can.indexOf(player.displayName);
			if (index > -1) hu.schedules[schedule].can.splice(index, 1);

			index = hu.schedules[schedule].maybe.indexOf(player.displayName);
			if (index > -1) hu.schedules[schedule].maybe.splice(index, 1);

			index = hu.schedules[schedule].alternate.indexOf(player.displayName);
			if (index > -1) hu.schedules[schedule].alternate.splice(index, 1);

			// Remove role of member
			let role = message.guild.roles.cache.find(role => role.name === schedule);
			if (role !== undefined) {
				message.member.roles.remove(role).catch(console.error);
			}
		}

		// Show new hu and save message before deleting past message
		message.channel.send(`${player.displayName} pulled down their hand!`, handsup(hu) )
			.then(newMessage => {
				if (hu.last_message === undefined) hu.last_message = {};
				if (hu.last_message[message.channel.id] !== undefined) {
					message.channel.messages.fetch(hu.last_message[message.channel.id])
						.then(oldMessage => {
							oldMessage.delete();
						});
				}
				hu.last_message[message.channel.id] = newMessage.id;

				// Save the new handsup
				hus[guild] = hu;
				saveHandsup(hus);
			});

		// Remove author's message
		message.delete();
	},
};
