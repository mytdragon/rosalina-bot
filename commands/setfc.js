const userHelper = require("../helpers/user.js");
const embedHelper = require("../helpers/embed.js");
const config = require('../config.json');
const axios = require('axios');

module.exports = {
	name: 'setfc',
	description: 'Set your switch fc.',
	usage: '<switch fc>',
	args: true,
    example: '$setfc 000-0000-0000',
	async execute(message, args) {

        let user = await userHelper.getUser({discord_id: message.author.id});
		if (!user) {
            message.channel.send(embedHelper.notRegisteredError());
			return;
		}

        axios({
            method: 'put',
            url: config['mk8d-timetrial-url'] + '/users/' + user.id,
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + config['mk8d-timetrial-token']
            },
            data: {
                switch_fc: args[0]
            }
        })
            .then(function (response) {
				message.channel.send( embedHelper.successfull(
					"Success",
					"Switch FC successfully updated."
				));
            })
            .catch(function (error) {
				let title = 'Cannot update the switch FC';
                switch (error.response.status) {
                    case 404:
                        message.channel.send( embedHelper.singleError(title, error.response.data.message) );
                        break;
                    case 422:
                        message.channel.send( embedHelper.validationError(title, error.response.data) );
                        break;
                    default:
						message.channel.send( embedHelper.singleError(
							title,
							'An unknown error has occured while updating the FC.'
						));
                }
            });
	}
};
