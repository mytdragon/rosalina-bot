const userHelper = require("../helpers/user.js");
const embedHelper = require("../helpers/embed.js");
const config = require('../config.json');
const axios = require('axios');

module.exports = {
	name: 'ttadd',
	aliases: ['tta'],
	description: 'Add a timetrial.',
	usage: '-cc <150|200> -track <code|name> -time <m:ss.mss> [-splits <split1> <split2> <split3>] [-character <name>] [-body <name>] [-tire <name>] [-glider <name>] [-video <url>]',
    example: '$ttadd -cc 150 -track dnbc -time 1:46.901 -splits 0:37.046 0:34.985 0:34.870 -character Rosalina -body Streetle -tire Azure Roller -glider Peach Parasol',
	async execute(message, args) {

        let user = await userHelper.getUser({discord_id: message.author.id});
		if (!user) {
            message.channel.send(embedHelper.notRegisteredError());
			return;
		}

        args['splits'] = args['splits'] ? args['splits'].split(/ +/) : null;

        axios.post(config['mk8d-timetrial-url'] + '/timetrials', {
                cc: args['cc'],
                track: args['track'],
                time: args['time'],
                video: args['video'],
                splits: args['splits'],
                character: args['character'],
                body: args['body'],
                tire: args['tire'],
                glider: args['glider'],
                player: user.id
            },
            {
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                    'Authorization': 'Bearer ' + config['mk8d-timetrial-token']
                }
            })
            .then(function (response) {
				message.channel.send( embedHelper.timetrial( response.data.timetrial ) );
            })
            .catch(function (error) {
				let title = 'Cannot add the time';
                switch (error.response.status) {
					case 400:
					case 404:
					case 409:
                        message.channel.send( embedHelper.singleError(title, error.response.data.message) );
                        break;
                    case 422:
                        message.channel.send( embedHelper.validationError(title, error.response.data) );
                        break;
                    default:
						message.channel.send( embedHelper.singleError(
							title,
							'An unknown error has occured while adding the time.'
						));
                }
            });


	},
};
