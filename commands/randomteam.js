const { singleError, teams } = require("../helpers/embed.js");
const config = require('../config.json');

const collect = require('collect.js');

module.exports = {
	name: 'randomteam',
	aliases: ['randteam', 'rand', 'rt'],
	description: 'Randomize players to different teams.',
	usage: '<number teams> <player> ... <player>',
	args: true,
    example: '$setfc 000-0000-0000',
	async execute(message, args) {

        let number = args[0];
		let players = args.slice(1);

		if (!Number.isInteger(parseInt(number))) { return message.channel.send( singleError('An error has occured', 'You need to enter a valid number of teams.')); }
		if (players.length % 2 != 0) { return message.channel.send( singleError('An error has occured', 'You need to have a even number of players.')); }

		let teamsShuffled = collect(players).shuffle().split(number);

		return message.channel.send( teams(teamsShuffled));
	}
};
