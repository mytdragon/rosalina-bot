const Discord = require('discord.js');

const { handsup: handsupConfig } = require('../config.json');
const { guildNotAuthorizedError, successfull } = require("../helpers/embed.js");

module.exports = {
	name: 'hutag',
	description: 'Toggle the tag role for a user.',
	usage: '',
	example: '$hutag',
	async execute(message, args) {
		// Check if server is authorized
		let guild = message.guild.id;
		if (!handsupConfig.authorizedGuilds.includes(guild)) {
			message.channel.send( guildNotAuthorizedError() );
			return;
		}

		// Check if `hutag` role exists and set to user
		let role = message.guild.roles.cache.find(role => role.name === 'hutag');
		if (role === undefined) {
			message.guild.roles.create({data: {name: 'hutag', mentionable: true}})
				.catch(console.error)
				.then(role => {
					// If user has role remove it else add it
					if (message.member.roles.cache.has(role.id)) message.member.roles.remove(role).catch(console.error);
					else message.member.roles.add(role).catch(console.error);
				});
		}
		else {
			// If user has role remove it else add it
			if (message.member.roles.cache.has(role.id)) message.member.roles.remove(role).catch(console.error);
			else message.member.roles.add(role).catch(console.error);
		}

		message.channel.send(successfull('Role toggle', 'The tag role has been toggled to you.'));
	},
};
