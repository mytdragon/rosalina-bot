const Discord = require('discord.js');

const { handsup: handsupConfig } = require('../config.json');
const { handsup, guildNotAuthorizedError, singleError } = require("../helpers/embed.js");
const { getHandsup, saveHandsup } = require("../helpers/data.js");

module.exports = {
	name: 'huout',
    aliases: ['out'],
	description: 'Remove schedules for the hands up feature.',
	usage: '<schedule> ... <schedule>',
	example: 'out 23',
	args: true,
	async execute(message, args) {
		// Check if server is authorized
		let guild = message.guild.id;
		if (!handsupConfig.authorizedGuilds.includes(guild)) {
			message.channel.send( guildNotAuthorizedError() );
			return;
		}

		// Get handsup for the server
		let hus = getHandsup();
		let hu = hus[guild];
		if (hu === undefined || hu.length === 0) {
			message.channel.send( singleError('No schedule set', 'You can set schedule with the command `huset`. Use help to get more details.') );
			return;
		}

		// Remove the schedules
		for (schedule of args) {
			delete hu.schedules[schedule];

			// Remove schedule role
			let role = message.guild.roles.cache.find(role => role.name === schedule);
			if (role !== undefined) {
				role.delete({data: {name: schedule}})
					.catch(console.error)
			}
		}

		// Show new hu and save message before deleting past message
		message.channel.send( handsup(hu) )
			.then(newMessage => {
				if (hu.last_message === undefined) hu.last_message = {};
				if (hu.last_message[message.channel.id] !== undefined) {
					message.channel.messages.fetch(hu.last_message[message.channel.id])
						.then(oldMessage => {
							oldMessage.delete();
						});
				}
				hu.last_message[message.channel.id] = newMessage.id;

				// Save the new handsup
				hus[guild] = hu;
				saveHandsup(hus);
			});

		// Remove author's message
		message.delete();
	},
};
