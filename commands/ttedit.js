const userHelper = require("../helpers/user.js");
const { singleError, timetrial, validationError } = require("../helpers/embed.js");
const config = require('../config.json');

const axios = require('axios');
const collect = require('collect.js');

module.exports = {
	name: 'ttedit',
	aliases: ['tte'],
	description: 'Edit a timetrial that matchs the filter given. If multiple timetrials are found, it edits the last added.',
	usage: '-track <code|name> -cc <150|200> [-time <m:ss.mss>] [-splits <split1> <split2> <split3>] [-character <name>] [-body <name>] [-tire <name>] [-glider <name>] [-video <url>]',
	args: true,
	cooldown: 60,
	example: '$ttedit -cc 150 -track dnbc -character King Boo -body streetle -tire Azure Roller -glider Cloud Glider -splits 0:37.046 0:34.985 0:34.870',
	async execute(message, args) {
		if (!args['track']) return message.channel.send( singleError('Argument missing', 'Argument `track` is required.') );
		if (!args['cc']) return message.channel.send( singleError('Argument missing', 'Argument `cc` is required.') );
		args['splits'] = args['splits'] ? args['splits'].split(/ +/) : null;

		let user = await userHelper.getUser({discord_id: message.author.id});
		if (!user) {
            message.channel.send(embedHelper.notRegisteredError());
			return;
		}

		let tt = await axios({
			method: 'get',
			url: config['mk8d-timetrial-url'] + '/timetrials',
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + config['mk8d-timetrial-token']
			},
			params: {
				cc: args['cc'],
				tracks: args['track'],
				users: user.id
			}
		})
			.then(function(response) {
				if(response.data.data.length == 0) {
					message.channel.send( singleError('No timetrial registered', 'There is no timetrial registered for that track and cc.') );
					return false;
				}

				return timesFilter = collect(response.data.data)
					.sortByDesc('created_at')
					.first();
			})
			.catch(function (error) {
				message.channel.send( embedHelper.singleError(
					'Oops...',
					'An unknown error has occured while retrieving your timetrial to edit.'
				));
				return false;
            });

		if (!tt) return;

		axios({
			method: 'put',
			url: config['mk8d-timetrial-url'] + '/timetrials/' + tt.id,
			headers: {
				'Accept': 'application/json',
				'Content-Type': 'application/json',
				'Authorization': 'Bearer ' + config['mk8d-timetrial-token']
			},
			params: {
				'time': args['time'],
	            'splits': args['splits'],
	            'character': args['character'],
	            'body': args['body'],
	            'tire': args['tire'],
	            'glider': args['glider'],
	            'video': args['video']
			}
		})
			.then(function(response) {
				message.channel.send( timetrial( response.data.timetrial ) );
			})
			.catch(function (error) {
				let title = 'Cannot edit the time';
                switch (error.response.status) {
					case 400:
					case 404:
					case 409:
                        message.channel.send( singleError(title, error.response.data.message) );
                        break;
                    case 422:
                        message.channel.send( validationError(title, error.response.data) );
                        break;
                    default:
						message.channel.send( singleError(
							title,
							'An unknown error has occured while editing the timetrial.'
						));
                }
            });
	}
};
