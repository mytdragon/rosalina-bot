const userHelper = require("../helpers/user.js");
const embedHelper = require("../helpers/embed.js");
const config = require('../config.json');

const axios = require('axios');
const collect = require('collect.js');

const Paginate = require('../class/Paginate.js');

module.exports = {
	name: 'ttlookup',
	aliases: ['ttl'],
	description: 'View timetrials.',
	usage: '[-player <pseudo> ... <pseudo>] [-track <code|name> ... <code|name>] [-cc <150|200>]',
	args: true,
	cooldown: 10,
    example: 'ttlookup -player @Rosalina',
	async execute(message, args) {

		if (args[0]) {
			args['player'] = args[0];
		}
		else {
			args['player'] = args['player'] ? args['player'].split(/ +/) : null;
		}

		args['track'] = args['track'] ? args['track'].split(/ +/) : null;

		let data = {};
		if (args['cc']) { data.cc = args['cc']; }
		if (args['player']) { data.users = args['player']; }
		if (args['track']) { data.tracks = args['track']; }

        axios({
            method: 'get',
            url: config['mk8d-timetrial-url'] + '/timetrials/',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + config['mk8d-timetrial-token']
            },
            params: data
        })
            .then(function (response) {
				if(response.data.data.length == 0) {
					message.channel.send( embedHelper.successfull('0 result', 'There is no time to show.') );
					return;
				}

				let timesFilter = collect(response.data.data)
					.sortByDesc('created_at')
					.sortBy('track.id')
					.sortBy('user.pseudo')
					.sortBy('cc')
					.unique(item => item.cc + item.track.id + item.user.id)
					.toArray();

				let pages = timesFilter.map((tt) => {
					return embedHelper.timetrial( tt );
				});

				let paginate = new Paginate(pages);
				paginate.send(message);
            })
            .catch(function (error) {
				message.channel.send( embedHelper.singleError(
					'Oops...',
					'An unknown error has occured while retrieving your timetrials.'
				));
            });
	}
};
