const axios = require('axios');

const { profile, singleError } = require("../helpers/embed.js");

module.exports = {
	name: 'mkcplayer',
    aliases: ['mkcp'],
	description: 'Get a player\'s mkcentral registry profile.',
	usage: '<player registry name>',
	args: true,
    example: 'mkcp MYT',
	async execute(message, args) {

		axios.get(`https://www.mariokartcentral.com/mkc/api/registry/players/category/all?search=${encodeURIComponent(args[0])}`)
			.then(function (response) {
				let players = response.data.data;
				let playerId = null;
				let regexp = new RegExp(`^${args[0]}$`, 'i');
				for (let item in players) {
			        if (regexp.test(players[item].display_name)) playerId = players[item].player_id;
			    }
				if (playerId === null) return message.channel.send(singleError('Player not found', `Player \`${args[0]}\` has not been found on Mario Kart Central registry.`));

				axios.get(`https://www.mariokartcentral.com/mkc/api/registry/players/${playerId}`)
					.then(function (response) {
						message.channel.send(profile(response.data, response.data.profile_picture, true));
		            })
					.catch(function (error) {
						console.error(error);
						message.channel.send( singleError(
							'An unknown error has occured',
							'An unknown error has occured while getting that player\'s mkc registry.'
						));
		            });
			})
			.catch(function (error) {
				console.error(error);
				message.channel.send( singleError(
					'An unknown error has occured',
					'An unknown error has occured while getting that player\'s mkc registry.'
				));
			});
	}
};
