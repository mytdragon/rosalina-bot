const embedHelper = require("../helpers/embed.js");
const config = require('../config.json');

const axios = require('axios');

module.exports = {
	name: 'bodies',
	description: 'List bodies.',
	async execute(message, args) {

        axios({
            method: 'get',
            url: config['mk8d-timetrial-url'] + '/bodies/',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + config['mk8d-timetrial-token']
            }
        })
            .then(function (response) {
				const data = [];
				data.push(response.data.data.map(body => `${client.emojis.cache.find(emoji => emoji.name === body.name.replace(/[ \-()]+/g, '')) || ''} ${body.name}`).join('\n'));

				message.channel.send(embedHelper.successfull('Bodies', data.join('\n')));
            })
            .catch(function (error) {
				let title = 'Oops...';
				if (error.response) {
	                switch (error.response.status) {
	                    case 404:
	                        message.channel.send( embedHelper.singleError(title, error.response.data.message) );
	                        break;
	                    case 422:
	                        message.channel.send( embedHelper.validationError(title, error.response.data) );
	                        break;
	                    default:
							message.channel.send( embedHelper.singleError(
								title,
								'An unknown error has occured while retrieving bodies.'
							));
	                }
				}
				else {
					console.log(error)
					message.channel.send( embedHelper.singleError(
						title,
						'An unknown error has occured while retrieving bodies.'
					));
				}
            });
	}
};
