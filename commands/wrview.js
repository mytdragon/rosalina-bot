const config = require('../config.json');
const axios = require('axios');
const { timetrial, singleError } = require("../helpers/embed.js");

const jsdom = require("jsdom");
const { JSDOM } = jsdom;

module.exports = {
	name: 'wrview',
    aliases: ['wr'],
	description: 'View a world record.',
    usage: '-track <code|name> [-cc <150|200]',
    args: true,
	async execute(message, args) {
        if (!args['track']) return message.channel.send( singleError('Argument missing', 'Argument `track` is required.') )
        if (!args['cc']) args['cc'] = '150';

        let track = await axios({
            method: 'get',
            url: config['mk8d-timetrial-url'] + '/tracks/' + args['track'],
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + config['mk8d-timetrial-token']
            }
        })
            .then(function (response) {
                return response.data.data;
            })
            .catch(function (error) {
                let title = 'Cannot get the track';
                switch (error.response.status) {
                    case 404:
                        message.channel.send( singleError(title, error.response.data.message) );
                        break;
                    case 422:
                        message.channel.send( singleError(title, error.response.data) );
                        break;
                    default:
                        message.channel.send( singleError(
                            title,
                            'An unknown error has occured getting the track.'
                        ));
                }
            });
        if (!track) return;

        const baseUrl = 'https://mkwrs.com/mk8dx/display.php?track=';
        const trackUrl = track.name.replace(/ /g, '+');
        const wrUrl = baseUrl + trackUrl + (args['cc'] == '200' ? '&m=200' : '')

        let res = await axios.get(wrUrl);
        const dom = new JSDOM(res.data);
        const document = dom.window.document;
        let trWr = document.querySelector("#main table.wr tbody tr:nth-of-type(2)");
        let tds = trWr.getElementsByTagName("td");

        let time = tds[1].firstChild.tagName ? tds[1].firstChild.innerHTML : tds[1].innerHTML;
        let video = tds[1].firstChild.tagName ? tds[1].firstChild.getAttribute("href") : null;
        let countryImgSrc = tds[3].firstChild.firstChild.getAttribute('src');
        let splits = [];
        for (let i = 0; i < track.laps; i++) {
            splits.push({ time: tds[5 + i].innerHTML.length == 6 ?  '0:' + tds[5 + i].innerHTML : tds[5 + i].innerHTML})
        }

        let wr = {
            date: tds[0].innerHTML,
            cc: args['cc'],
            track: track,
            time: time.replace("'", ':').replace('"', '.'),
            video: video,
            user: {
                pseudo: tds[2].firstChild.innerHTML,
                country: {code: countryImgSrc.substr( countryImgSrc.lastIndexOf('/') + 1, 2)}
            },
            splits: splits,
            character: { name: tds[5 + track.laps + 2].innerHTML },
            body: { name: tds[5 + track.laps + 3].innerHTML },
            tire: { name: tds[5 + track.laps + 4].innerHTML },
            glider: { name: tds[5 + track.laps + 5].innerHTML }
        }

        message.channel.send( timetrial(wr) );
	},
};
