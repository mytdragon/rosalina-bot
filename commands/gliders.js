const embedHelper = require("../helpers/embed.js");
const config = require('../config.json');

const axios = require('axios');

module.exports = {
	name: 'gliders',
	description: 'List gliders.',
	async execute(message, args) {

        axios({
            method: 'get',
            url: config['mk8d-timetrial-url'] + '/gliders/',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + config['mk8d-timetrial-token']
            }
        })
            .then(function (response) {
				const data = [];
				data.push(response.data.data.map(glider => `${client.emojis.cache.find(emoji => emoji.name === glider.name.replace(/[ \-()]+/g, '')) || ''} ${glider.name}`).join('\n'));

				message.channel.send(embedHelper.successfull('gliders', data.join('\n')));
            })
            .catch(function (error) {
				let title = 'Oops...';
				if (error.response) {
	                switch (error.response.status) {
	                    case 404:
	                        message.channel.send( embedHelper.singleError(title, error.response.data.message) );
	                        break;
	                    case 422:
	                        message.channel.send( embedHelper.validationError(title, error.response.data) );
	                        break;
	                    default:
							message.channel.send( embedHelper.singleError(
								title,
								'An unknown error has occured while retrieving gliders.'
							));
	                }
				}
				else {
					console.log(error)
					message.channel.send( embedHelper.singleError(
						title,
						'An unknown error has occured while retrieving gliders.'
					));
				}
            });
	}
};
