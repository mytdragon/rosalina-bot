const Discord = require('discord.js');

const { handsup: handsupConfig } = require('../config.json');
const { guildNotAuthorizedError, successfull, singleError } = require("../helpers/embed.js");
const { getHandsup, saveHandsup } = require("../helpers/data.js");

module.exports = {
	name: 'huping',
	description: 'Set the number of player required to ping. Default is `3`.',
	usage: '<number>',
	example: '$huping 4',
	args: true,
	async execute(message, args) {
		// Check if server is authorized
		let guild = message.guild.id;
		if (!handsupConfig.authorizedGuilds.includes(guild)) {
			message.channel.send( guildNotAuthorizedError() );
			return;
		}

		// Get handsup for the server
		let hus = getHandsup();
		let hu = hus[guild];
		if (hu === undefined) hu = hus[guild] = {};
		if (hu.schedules === undefined) hu.schedules = {};
		if (hu.ping === undefined) hu.ping = 3;

		// set ping
		if (isNaN(args[0])) return message.channel.send( singleError('Not a number', 'The argument must be a number.') );
		hu.ping = parseInt(args[0]);

		// Save the new handsup
		hus[guild] = hu;
		saveHandsup(hus);

		message.channel.send(successfull('Ping rule updated', 'The ping rule has been updated.'));
	},
};
