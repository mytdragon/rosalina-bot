const Discord = require('discord.js');

const { handsup: handsupConfig } = require('../config.json');
const { handsup, guildNotAuthorizedError } = require("../helpers/embed.js");
const { getHandsup, saveHandsup } = require("../helpers/data.js");

module.exports = {
	name: 'huset',
    aliases: ['set'],
	description: 'Set schedules for the hands up feature. It does not override the past schedules set.',
	usage: '<schedule> ... <schedule>',
	example: '$set 20 21 22 23',
	args: true,
	async execute(message, args) {
		// Check if server is authorized
		let guild = message.guild.id;
		if (!handsupConfig.authorizedGuilds.includes(guild)) {
			message.channel.send( guildNotAuthorizedError() );
			return;
		}

		// Get handsup for the server
		let hus = getHandsup();
		let hu = hus[guild];
		if (hu === undefined) hu = hus[guild] = {};
		if (hu.schedules === undefined) hu.schedules = {};
		if (hu.ping === undefined) hu.ping = 3;

		for (schedule of args) {
			// Initialize the shedules
			hu.schedules[schedule] = {
				"can": [],
				"maybe": [],
				"alternate": [],
				"opponent": null,
				"host": null,
				"open": null
			};

			// Remove role if exist for whatever reason
			let roleToDelete = message.guild.roles.cache.find(role => role.name === schedule);
			if (roleToDelete !== undefined) {
				await roleToDelete.delete({data: {name: schedule}})
					.catch(console.error)
			}

			// Create schedule role
			let role = message.guild.roles.cache.find(role => role.name === schedule);
			if (role === undefined) {
				message.guild.roles.create({data: {name: schedule, mentionable: true}})
					.catch(console.error)
			}
		}

		// Show new hu and save message before deleting past message
		message.channel.send( handsup(hu) )
			.then(newMessage => {
				if (hu.last_message === undefined) hu.last_message = {};
				if (hu.last_message[message.channel.id] !== undefined) {
					message.channel.messages.fetch(hu.last_message[message.channel.id])
						.then(oldMessage => {
							oldMessage.delete();
						});
				}
				hu.last_message[message.channel.id] = newMessage.id;

				// Save the new handsup
				hus[guild] = hu;
				saveHandsup(hus);
			});

		// Remove author's message
		message.delete();
	},
};
