const embedHelper = require("../helpers/embed.js");
const config = require('../config.json');
const axios = require('axios');

module.exports = {
	name: 'register',
	description: 'Register using your discord ID.',
	usage: '-pseudo <pseudo> -country <name|code>',
	args: true,
	async execute(message, args) {

		axios({
            method: 'post',
            url: config['mk8d-timetrial-url'] + '/auth/signup/discord',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + config['mk8d-timetrial-token']
            },
            data: {
                discord_id: message.author.id,
				pseudo: args['pseudo'],
				country: args['country']
            }
        })
            .then(function (response) {
				message.channel.send( embedHelper.successfull(
					"Welcome !",
					"You have been successfully registered."
				));
            })
            .catch(function (error) {
				let title = 'Cannot register you';
                switch (error.response.status) {
                    case 404:
					case 409:
                        message.channel.send( embedHelper.singleError(title, error.response.data.message) );
                        break;
                    case 422:
                        message.channel.send( embedHelper.validationError(title, error.response.data) );
                        break;
                    default:
						console.log(error);
						message.channel.send( embedHelper.singleError(
							title,
							'An unknown error has occured while registering you.'
						));
                }
            });
	},
};
