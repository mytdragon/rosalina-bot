const userHelper = require("../helpers/user.js");
const embedHelper = require("../helpers/embed.js");
const config = require('../config.json');

const axios = require('axios');
const collect = require('collect.js');

const Paginate = require('../class/Paginate.js');

module.exports = {
	name: 'ttview',
	aliases: ['tt'],
	description: 'View your timetrials.',
    example: '$tt',
	async execute(message, args) {

        let user = await userHelper.getUser({discord_id: message.author.id});
		if (!user) {
            message.channel.send(embedHelper.notRegisteredError());
			return;
		}

        axios({
            method: 'get',
            url: config['mk8d-timetrial-url'] + '/timetrials/',
            headers: {
                'Accept': 'application/json',
                'Content-Type': 'application/json',
                'Authorization': 'Bearer ' + config['mk8d-timetrial-token']
            },
            params: {
                users: user.id
            }
        })
            .then(function (response) {
				let timesFilter = collect(response.data.data)
					.sortByDesc('created_at')
					.sortBy('track.id')
					.sortBy('cc')
					.unique(item => item.cc + item.track.id)
					.toArray();

				let pages = timesFilter.map((tt) => {
					return embedHelper.timetrial( tt );
				});

				let paginate = new Paginate(pages);
				paginate.send(message);
            })
            .catch(function (error) {
				let title = 'Oops...';
				if (error.response) {
	                switch (error.response.status) {
	                    case 404:
	                        message.channel.send( embedHelper.singleError(title, error.response.data.message) );
	                        break;
	                    case 422:
	                        message.channel.send( embedHelper.validationError(title, error.response.data) );
	                        break;
	                    default:
							message.channel.send( embedHelper.singleError(
								title,
								'An unknown error has occured while retrieving your timetrials.'
							));
	                }
				}
				else {
					console.log(error)
					message.channel.send( embedHelper.singleError(
						title,
						'An unknown error has occured while retrieving your timetrials.'
					));
				}
            });
	}
};
