const { prefix } = require('../config.json');
const { successfull } = require("../helpers/embed.js");

module.exports = {
	name: 'help',
	aliases: ['commands'],
	description: 'List all of the commands or info about a specific command.',
	async execute(message, args) {

		const data = [];
		const { commands } = message.client;

		if (!args.length) {
			data.push(commands.map(command => `\`${command.name}\` ${command.description}`).join('\n'));
			data.push(`\nYou can send \`${prefix}help <command>\` to get info on a specific command.`);

			return message.channel.send( successfull('Here\'s a list of all the commands:', data.join('\n')) );

			// Send in private
			/*return message.author.send(data.join('\n'))
				.then(() => {
					if (message.channel.type === 'dm') return;
					message.reply('I\'ve sent you a DM with all my commands!');
				})
				.catch(error => {
					console.error(`Could not send help DM to ${message.author.tag}.\n`, error);
					message.reply('it seems like I can\'t DM you! Do you have DMs disabled?');
				});*/
		}

		const name = args[0].toLowerCase();
		const command = commands.get(name) || commands.find(c => c.aliases && c.aliases.includes(name));
		if (!command) return message.reply('that\'s not a valid command.');

		data.push(`**Name:** ${command.name}`);
		if (command.aliases) data.push(`**Aliases:** \`${command.aliases.join('`, `')}\``);
		if (command.description) data.push(`**Description:** ${command.description}`);
		if (command.usage) data.push(`**Usage:** \`${prefix}${command.name} ${command.usage}\``);
		if (command.example) data.push(`**Example:** \`${command.example}\``);
		if (command.cooldown) data.push(`**Cooldown:** ${command.cooldown} second(s)`);

		message.channel.send( successfull(command.name, data.join('\n')) );
	},
};
