const Discord = require('discord.js');
const fs = require('fs');

const embedHelper = require("./helpers/embed.js");

const config = require('./config.json');

// Create data folder if not exist
const dataDir = './data';
if (!fs.existsSync(dataDir)){
    fs.mkdirSync(dataDir);
}

global.client = new Discord.Client();
client.commands = new Discord.Collection();
const commandFiles = fs.readdirSync('./commands').filter(file => file.endsWith('.js'));

for (const file of commandFiles) {
	const command = require(`./commands/${file}`);
	client.commands.set(command.name, command);
}

const cooldowns = new Discord.Collection();

process.on('unhandledRejection', error => {
	console.error('Unhandled promise rejection:', error);
});

client.on('ready', () => {
    console.log(`Logged in as ${client.user.tag}!`);
});

client.on('message', message => {
    if ( !message.content.startsWith(config.prefix) ) return;
    if ( message.channel instanceof Discord.DMChannel ) return;

	if (message.content.match(/ -[a-zA-z]/)){
		// Args with param name (eg: $sayhello -name Toto)
		var args = message.content.slice(config.prefix.length).split(/ -+/);
		var commandName = args.shift().toLowerCase();
		var _args = new Array();
		args.forEach ( (element) => {
			element = element.split(/ (.+)/).filter(x => x);
			_args[ element[0] ] = element[1];
		})
		args = _args
	}
	else {
		// Args with no param (eg: $sayhello Toto)
		var args = message.content.slice(config.prefix.length).split(/ +/);
		var commandName = args.shift().toLowerCase();
	}

	const command = client.commands.get(commandName)
		|| client.commands.find(cmd => cmd.aliases && cmd.aliases.includes(commandName));
	if (!command) return;

	if (command.args && !(args.length || Object.keys(args).length)) {
		let description = `You didn't provide any arguments.`

		if (command.usage) {
			description += `\nThe proper usage would be: \`${config.prefix}${command.name} ${command.usage}\``;
		}

		return message.channel.send( embedHelper.singleError('Arguments missing', description) );
	}

	if (command.guildOnly && message.channel.type !== 'text') {
		return message.reply( embedHelper.singleError('Command error', 'You can\'t use that command inside DMs.') );
	}

	if (!cooldowns.has(command.name)) {
		cooldowns.set(command.name, new Discord.Collection());
	}

	const now = Date.now();
	const timestamps = cooldowns.get(command.name);
	const cooldownAmount = (command.cooldown || 0) * 1000;

	if (timestamps.has(message.author.id)) {
		const expirationTime = timestamps.get(message.author.id) + cooldownAmount;

		if (now < expirationTime) {
			const timeLeft = (expirationTime - now) / 1000;
			return message.channel.send( embedHelper.singleError('On cooldown', `please wait ${timeLeft.toFixed(1)} more second(s) before reusing the \`${command.name}\` command.`) );
		}
	}

	timestamps.set(message.author.id, now);
	setTimeout(() => timestamps.delete(message.author.id), cooldownAmount);

    try {
    	command.execute(message, args);
    } catch (error) {
    	console.error(error);
		message.channel.send( embedHelper.singleError('Command error', 'there was an error trying to execute that command.') );
    }
});

client.login(config.token);
