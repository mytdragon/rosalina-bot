// TUTO : https://stackoverflow.com/questions/57324773/how-to-edit-message-according-to-reaction-in-discord-js-create-a-list-and-switc
module.exports = class Paginate {

    constructor(embeds) {
        this.embeds = embeds;

        this.time = 60000 * 5; // time limit: 60000 = 1 min
        this.emojiNext = '➡'; // unicode emoji are identified by the emoji itself
        this.emojiPrevious = '⬅';
        this.reactionArrow = [this.emojiPrevious, this.emojiNext];
    }

    getList(i) {
        let footer = this.embeds[i].embed.footer;
        if (!footer.text.includes('Page')) {
            this.embeds[i].embed.footer = {
                text: `Page ${i+1}/${this.embeds.length} | ${footer.text}`
            };
        }
        return this.embeds[i];
    }

    onCollect(emoji, message, i) {
        if ((emoji.name === this.emojiPrevious) && (i > 0)) {
            message.edit(this.getList(--i));
        }
        else if ((emoji.name === this.emojiNext) && (i < this.embeds.length-1)) {
            message.edit(this.getList(++i));
        }
        return i;
    }

    createCollectorMessage(message) {
        let i = 0;
        const filter = (reaction, user) => {
            // check if the emoji is inside the list of emojis, and if the user is not a bot
            return (!user.bot) && (this.reactionArrow.includes(reaction.emoji.name));
        }
        const collector = message.createReactionCollector(filter, { time: this.time });
        collector.on('collect', r => {
            i = this.onCollect(r.emoji, message, i, this.embeds, this.getList);
        });
        collector.on('end', collected => message.clearReactions());
    }

    send(message) {
        message.channel.send(this.getList(0))
            .then(msg => msg.react(this.emojiPrevious))
            .then(msgReaction => msgReaction.message.react(this.emojiNext))
            .then(msgReaction => this.createCollectorMessage(msgReaction.message, this.embeds.length, this.getList));
    }
}
